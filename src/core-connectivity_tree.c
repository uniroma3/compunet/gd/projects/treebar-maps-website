#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define V 68349500;    
#define E 1811849350; 

// OUTPUT STRUCTURES
// core-connectivity tree structure
typedef struct node_tree_struct
{
    int id;
    int coreness;
    int range_c[2];
    int n_numerousness;
    int e_numerousness;
    int n_tm_numerousness;
    int e_tm_numerousness;
    int depth;
    struct node_tree_struct *child;
    struct node_tree_struct *last_child;
    struct node_tree_struct *sibling;
    struct node_tree_struct *prev_sibling;
    struct node_tree_struct *parent;
    struct node_ds_struct *node_set;
} node_tree;

// support disjoint set structure
typedef struct node_ds_struct
{
    int id;
    int rank;
    int coreness;
    struct node_ds_struct *parent;
    node_tree *root_tree;
} node_ds;

// INPUT STRUCTURES

// adjacency list struct
typedef struct neighbour_struct
{
    int id;
    struct neighbour_struct *next;
} neighbour;

typedef struct vertex_struct
{
    int id;
    int coreness;
    int degree;
    node_tree *v_tree;
    neighbour *first_neighbour;
} vertex;

typedef struct edge_struct
{
    int coreness;
    vertex *a;
    vertex *b;
} edge;

// vertices and edges are stored into two arrays; the graph structure has a pointer for each array.
typedef struct graph_struct
{
    int num_edges;
    int num_vertices;
    int len_v;
    int len_e;
    edge *edges;
    vertex *vertices;
} graph;

//************************************************************SETTING GRAPH INPUT************************************************************//

graph *set_input(char *input_path)
{   
    int number_of_vertices = 0;
    int number_of_edges = 0;
    // Read graph size
    FILE *file = fopen(input_path, "r");
    if(file!=NULL){
        char line[1000];
        while (fgets(line, sizeof(line), file))
        {
            int start, end;
            if (sscanf(line, "%d %d", &start, &end) == 2)
            {
                if(start>number_of_vertices)
                    number_of_vertices=start;
                if(end>number_of_vertices)
                    number_of_vertices=end;
                number_of_edges++;
            }
        }
        fclose(file);
    }
    else
    {
        printf("%s: file not found\n", input_path);
        exit(1);
    }
    
    // Read graph

    file = fopen(input_path, "r");
    if (file != NULL)
    {
        graph *g = (graph *)malloc(sizeof(graph));
        vertex *v_array = (vertex *)calloc(number_of_vertices, sizeof(vertex));
        edge *e_array = (edge *)calloc(number_of_edges, sizeof(edge));

        if (g == NULL || v_array == NULL || e_array == NULL)
        {
            printf("memory allocation failed\n");
            exit(1);
        }

        g->len_v = number_of_vertices;
        g->vertices = v_array;

        g->len_e = number_of_edges;
        g->edges = e_array;

        int len_list_edges = 0;
        int len_list_vertex = 0;
        int c = 0;

        char line[1000];

        vertex *v_start, *v_end;

       

        while (fgets(line, sizeof(line), file))
        {
            int start, end;
            if (sscanf(line, "%d %d", &start, &end) == 2)
            {
                if (start != end)
                { // esegui la computazione solo se l'arco non è un loop
                    c++;
                    if (start > len_list_vertex)
                    {
                        len_list_vertex = start;
                    }
                    if (end > len_list_vertex)
                    {
                        len_list_vertex = end;
                    }

                    v_start = &(g->vertices[start]);
                    v_end = &(g->vertices[end]);

                    if (v_start->id != 0)
                    {
                        v_start->degree++;
                    }
                    else
                    {
                        v_start->id = start;
                        v_start->degree = 1;
                    }

                    if (v_end->id != 0)
                    {
                        v_end->degree++;
                    }
                    else
                    {
                        v_end->id = end;
                        v_end->degree = 1;
                    }

                    neighbour *vic = (neighbour *)malloc(sizeof(neighbour));
                    neighbour *vic2 = (neighbour *)malloc(sizeof(neighbour));
                    if (vic == NULL || vic2 == NULL)
                    {
                        printf("neighbour memory allocation failed\n");
                    }

                    vic->id = v_end->id;
                    vic->next = v_start->first_neighbour;
                    v_start->first_neighbour = vic;

                    vic2->id = v_start->id;
                    vic2->next = v_end->first_neighbour;
                    v_end->first_neighbour = vic2;

                    g->edges[len_list_edges].a = v_start;
                    g->edges[len_list_edges].b = v_end;

                    len_list_edges++;

                }
            }
        }
        fclose(file);
        g->num_edges = len_list_edges;
        g->num_vertices = len_list_vertex;
        return g;
    }
    else
    {
        printf("%s: file not found\n", input_path);
        exit(1);
    }
}

void compute_coreness(graph *graph)
{

    int n = graph->num_vertices;
    int md = 0;
    int d, i, start, num;
    int v, u, w, du, pu, pw;

    int *deg = malloc(sizeof(int) * (n + 1));
    int *bin = calloc(n + 1, sizeof(int));
    int *pos = malloc(sizeof(int) * (n + 1));
    int *vert = malloc(sizeof(int) * (n + 1));

    if (deg == NULL || bin == NULL || pos == NULL || vert == NULL)
    {
        printf("malloc compute_coreness fallito\n");
        exit(1);
    }

    for (v = 1; v <= n; v++)
    {
        vertex vertex = graph->vertices[v];
        d = vertex.degree;

        deg[v] = d;
        if (d > md)
            md = d;
        bin[deg[v]]++;
    }
    start = 1;
    for (d = 0; d <= md; d++)
    {
        num = bin[d];
        bin[d] = start;
        start += num;
    }

    for (v = 1; v <= n; v++)
    {
        pos[v] = bin[deg[v]];
        vert[pos[v]] = v;
        bin[deg[v]]++;
        deg[v] = graph->vertices[v].degree;
    }

    for (d = md; d >= 0; d--)
    {
        bin[d] = bin[d - 1];
    }
    bin[0] = 1;
    int num_vert = 0;

    for (i = 1; i <= n; i++)
    {
        num_vert++;
        v = vert[i];

        if (graph->vertices[v].id != 0)
        {

            neighbour *vi = graph->vertices[v].first_neighbour;
            for (int j = 0; j < graph->vertices[v].degree; j++)
            {

                vertex vic = graph->vertices[vi->id];
                u = vic.id;

                if (deg[u] > deg[v])
                {
                    du = deg[u];
                    pu = pos[u];
                    pw = bin[du];
                    w = vert[pw];

                    if (u != w)
                    {
                        pos[u] = pw;
                        vert[pu] = w;
                        pos[w] = pu;
                        vert[pw] = u;
                    }
                    bin[du]++;
                    deg[u]--;
                }

                vi = vi->next;
            }
        }
        graph->vertices[v].coreness = deg[v];
    }
    free(vert);
    free(pos);
    free(bin);
    free(deg);
}

//**************************************************SORT EDGES BY DECREASING CORENESS VALUE**************************************************//

int find_min(int a, int b)
{
    if (a <= b)
        return a;
    else
        return b;
}

void sort_edges(graph *graph)
{
    int max_c = 0;

    for (int l = 1; l <= graph->num_vertices; l++)
    {
        if (graph->vertices[l].coreness > max_c)
            max_c = graph->vertices[l].coreness;
    }
    int *aux = (int *)calloc(max_c + 1, sizeof(int));
    int *bucket = (int *)calloc(max_c + 1, sizeof(int));

    if (aux == NULL || bucket == NULL)
    {
        printf("malloc in sort_edges failed\n");
    }
    for (int i = 0; i < graph->num_edges; i++)
    {
        graph->edges[i].coreness = find_min(graph->edges[i].a->coreness, graph->edges[i].b->coreness);
        aux[graph->edges[i].coreness]++;
    }

    int size = graph->num_edges;

    for (int j = max_c - 1; j >= 1; j--)
    {
        bucket[j] = bucket[j + 1] + aux[j + 1];
    }
    free(aux);

    edge *edges = (edge *)malloc(size * sizeof(edge));

    for (int i = 0; i < size; i++)
    {
        int a = graph->edges[i].a->id;
        int b = graph->edges[i].b->id;
        int core_e = graph->edges[i].coreness;
        int posix = bucket[core_e];

        edges[posix].a = &(graph->vertices[a]);
        edges[posix].b = &(graph->vertices[b]);
        edges[posix].coreness = core_e;

        bucket[core_e]++;
    }

    free(graph->edges);
    free(bucket);
    graph->edges = edges;
}

//********************************************************CREATE LEAVES OF CCTREE AND DS********************************************************//

void makeSetLeaf(vertex *v)
{
    node_ds *n_ds = (node_ds *)calloc(1, sizeof(node_ds));
    n_ds->id = v->id;
    n_ds->coreness = v->coreness;
    n_ds->parent = n_ds;

    node_tree *n_tree = (node_tree *)calloc(1, sizeof(node_tree));
    n_tree->id = v->id;
    n_tree->node_set = n_ds;
    n_tree->coreness = v->coreness;
    v->v_tree = n_tree;
}

void init(graph *g)
{
    // Setup the CCTree and support Disjoint Set for each vertex of the input graph
    for (int i = 1; i <= g->num_vertices; i++)
    {
        makeSetLeaf(&(g->vertices[i]));
    }
}

//**********************************************************CORE-CONNECTIVITY TREE CONSTRUCTION**********************************************************//

node_tree *find_min_coreness(node_tree *a, node_tree *b)
{
    if (a->coreness <= b->coreness)
        return a;
    else
        return b;
}

node_tree *find_max_coreness(node_tree *a, node_tree *b)
{
    if (a->coreness <= b->coreness)
        return b;
    else
        return a;
}

node_ds *find(node_ds *n_set)
{
    while (n_set != n_set->parent)
    {
        n_set = n_set->parent;
    }
    return n_set;
}

void merge_ds(node_ds *root_a, node_ds *root_b, node_tree *k_tree)
{
    int rank_a = root_a->rank;
    int rank_b = root_b->rank;

    if (rank_a > rank_b)
    {
        root_b->parent = root_a;
        root_a->root_tree = k_tree;
    }
    else
    {
        root_a->parent = root_b;
        root_b->root_tree = k_tree;

        if (rank_a == rank_b)
            root_b->rank = root_b->rank + 1;
    }
}

node_tree *build(graph *g)
{
    node_tree *root = (node_tree *)calloc(1, sizeof(node_tree));
    int size = g->num_vertices;

    node_tree **array_internal_nodes = calloc(size, sizeof(node_tree));
    int num_internal_nodes = 0;

    int j = 1;
    int c = 0;
    int prevCoreness = g->edges->coreness;
    for (int i = 0; i < g->num_edges; i++)
    {

        c++;
        edge *edg_elm = &(g->edges[i]);

        vertex *a = edg_elm->a;
        vertex *b = edg_elm->b;

        node_tree *a_tree = a->v_tree;
        node_tree *b_tree = b->v_tree;

        node_ds *a_ds = a_tree->node_set;
        node_ds *b_ds = b_tree->node_set;

        node_ds *root_a_ds = find(a_ds);
        node_ds *root_b_ds = find(b_ds);

        node_tree *root_a_tree = root_a_ds->root_tree;
        node_tree *root_b_tree = root_b_ds->root_tree;

        int core_e = edg_elm->coreness;

        if (prevCoreness > core_e)
        {
            j = 1;
        }
        prevCoreness = core_e;

        if (root_a_tree == NULL && root_b_tree == NULL)
        {   node_tree *k_tree = (node_tree *)calloc(1, sizeof(node_tree));
            k_tree->id = j + (core_e * 1000);
            k_tree->coreness = core_e;

            k_tree->child = a_tree;
            k_tree->last_child = b_tree;
            a_tree->sibling = b_tree;
            b_tree->prev_sibling = a_tree;

            a_tree->parent = k_tree;
            b_tree->parent = k_tree;

            merge_ds(root_a_ds, root_b_ds, k_tree);
            
            array_internal_nodes[num_internal_nodes] = k_tree;
            num_internal_nodes++;
            j++;
        }

        else if (root_a_tree == NULL && !(root_b_tree == NULL))
        {
            if (core_e < root_b_tree->coreness)
            {
                node_tree *k_tree = (node_tree *)calloc(1, sizeof(node_tree));
                k_tree->id = j + (core_e * 1000);
                k_tree->coreness = core_e;

                k_tree->child = root_b_tree;
                k_tree->last_child = a_tree;
                root_b_tree->sibling = a_tree;
                a_tree->prev_sibling = root_b_tree;

                root_b_tree->parent = k_tree;
                a_tree->parent = k_tree;

                merge_ds(root_a_ds, root_b_ds, k_tree);
               
                array_internal_nodes[num_internal_nodes] = k_tree;
                num_internal_nodes++;
                j++;
            }
            else
            {   node_tree *last_child = root_b_tree->last_child;
                last_child->sibling = a_tree;
                a_tree->prev_sibling = last_child;
                root_b_tree->last_child = a_tree;

                a_tree->parent = root_b_tree;

                merge_ds(root_a_ds, root_b_ds, root_b_tree);
            }
        }

        else if (!(root_a_tree == NULL) && root_b_tree == NULL)
        {
            if (core_e < root_a_tree->coreness)
            {   node_tree *k_tree = (node_tree *)calloc(1, sizeof(node_tree));
                k_tree->id = j + (core_e * 1000);
                k_tree->coreness = core_e;

                k_tree->child = root_a_tree;
                k_tree->last_child = b_tree;
                root_a_tree->sibling = b_tree;
                b_tree->prev_sibling = root_a_tree;

                root_a_tree->parent = k_tree;
                b_tree->parent = k_tree;

                merge_ds(root_a_ds, root_b_ds, k_tree);
               
                array_internal_nodes[num_internal_nodes] = k_tree;
                num_internal_nodes++;
                j++;
            }
            else
            {   node_tree *last_child = root_a_tree->last_child;
                last_child->sibling = b_tree;
                b_tree->prev_sibling = last_child;
                root_a_tree->last_child = b_tree;

                b_tree->parent = root_a_tree;

                merge_ds(root_a_ds, root_b_ds, root_a_tree);
            }
        }

        else
        {
            if (root_a_tree != root_b_tree)
            {
                node_tree *root_min_tree = find_min_coreness(root_a_tree, root_b_tree);
                node_tree *root_max_tree = find_max_coreness(root_a_tree, root_b_tree);

                node_tree *last_child = root_min_tree->last_child;
                last_child->sibling = root_max_tree;
                root_max_tree->prev_sibling = last_child;
                root_min_tree->last_child = root_max_tree;

                root_max_tree->parent = root_min_tree;

                merge_ds(root_a_ds, root_b_ds, root_min_tree);
            }
        }
    }

    // connect nodes without parent to the root
    int first = 0;
    node_tree *sibling;
    for (int i = 0; i < num_internal_nodes; i++)
    {
        if (array_internal_nodes[i]->parent == NULL)
        {
            if (first == 0)
            {
                root->child = array_internal_nodes[i];
                root->child->prev_sibling = NULL;
                array_internal_nodes[i]->parent = root;
                first = 1;
                sibling = array_internal_nodes[i];
            }
            else
            {
                array_internal_nodes[i]->parent = root;
                sibling->sibling = array_internal_nodes[i];
                array_internal_nodes[i]->prev_sibling = sibling;
                sibling = array_internal_nodes[i];
            }
        }
        else
        {
        }
    }
    free(array_internal_nodes);
    return root;
}

//****************************************************************FIX CCTREE****************************************************************//

void action(node_tree *node /*, node_tree* parent, node_tree* first_child*/)
{

    node_tree *n_child = node->child;
    node_tree *n_parent = node->parent;

    // 1. delete node from parent's children
    if (node->prev_sibling == NULL)
    {
        n_parent->child = node->sibling;
        node->sibling->prev_sibling = NULL;
    }
    else if (node->sibling == NULL)
    {
        n_parent->last_child = node->prev_sibling;
        node->prev_sibling->sibling = NULL;
    }
    else
    {
        node_tree *prev_sibling = node->prev_sibling;
        node_tree *next_sibling = node->sibling;
        prev_sibling->sibling = next_sibling;
        next_sibling->prev_sibling = prev_sibling;
    }
    // 2. assign to node children the node parent as parent
    while (n_child != NULL)
    {
        n_child->parent = n_parent;
        n_child = n_child->sibling;
    }

    // unify children
    if (n_parent->child != NULL)
    {
        n_parent->last_child->sibling = node->child;
        n_parent->last_child = node->last_child;
    }
    else
    {
        n_parent->child = node->child;
        n_parent->last_child = node->last_child;
    }
}

void fix_tree(node_tree *root, int len)
{

    node_tree *node = root->child;
    int size = len * 2;
    node_tree **temp = malloc(sizeof(node_tree) * size);
    node_tree **array = malloc(sizeof(node_tree) * len);

    if (temp == NULL || array == NULL)
    {
        printf("malloc fix_tree failed\n");
    }

    int len_t = 0;
    int len_a = 0;
    int i = 0;

    while (1)
    {   if (node->sibling != NULL)
        {
            temp[len_t] = node->sibling;
            len_t++;
        }
        if (node->child != NULL)
        {
            node->range_c[0] = node->coreness;
            node->range_c[1] = node->parent->coreness + 1;
            temp[len_t] = node->child;
            len_t++;
        }
        else
        {
            free(node->node_set);
        }

        if (node->child != NULL && node->coreness == node->parent->coreness)
        {
            array[len_a] = node;
            len_a++;
        }

        if (len_t > i)
        {
            node = temp[i];
            i++;
        }
        else
            break;
    }
    free(temp);
    for (int j = 0; j < len_a; j++)
    {
        action(array[j]);
    }
    free(array);
}

//****************************************************************PRIMITIVES****************************************************************//

// PRIMITIVES ON THE INPUT GRAPH

void num_vertices_edges(graph *graph)
{
    printf("The number of edges is %d\n", graph->num_edges);
    printf("The number of vertices is %d\n", graph->num_vertices);
}

void print_vertices(graph *graph)
{
    int len_list_vertex = graph->num_vertices;
    printf("\nVertex List:\n");
    for (int i = 1; i <= len_list_vertex; i++)
    {
        printf("vertex: %d\t", graph->vertices[i].id);
        printf("coreness: %d\t", graph->vertices[i].coreness);
        printf("degree: %d\t", graph->vertices[i].degree);
        printf("neighbours: ");
        neighbour *vic = graph->vertices[i].first_neighbour;
        for (int j = 0; j < graph->vertices[i].degree; j++)
        {
            printf("%d ", vic->id);
            vic = vic->next;
        }
        printf("\n");
    }
}

void print_edges(graph *graph)
{
    printf("Edge List:\n");
    edge *temp_e = graph->edges;
    int len_list_edges = graph->num_edges;
    for (int i = 0; i < len_list_edges; i++)
    {
        printf("edge: %d, %d\tcoreness: %d\n", temp_e[i].a->id, temp_e[i].b->id, temp_e[i].coreness);
    }
}

void lv_coreness_max_min(graph *graph)
{
    int val = graph->edges->coreness;
    printf("The maximum coreness value is %d\n", val);

    for (int i = 1; i <= graph->num_vertices; i++)
    {
        if (graph->vertices[i].coreness < val)
            val = graph->vertices[i].coreness;
    }
    printf("The minimum coreness value is %d\n", val);
}

// PRIMITIVES ON THE CORE-CONNECTIVITY TREE

void printTabs(int count)
{
    for (int i = 0; i < count; i++)
    {
        putchar('\t');
    }
}

void print(node_tree *node, int level)
{
    while (node != NULL)
    {

        if (node->child == NULL)
        {
            printTabs(level);
            printf("Node: %d\n", node->id);
        }
        else
        {
            printTabs(level);
            printf("Node: %d, [%d,%d], [n:%d,%d - e:%d,%d]\n", node->id, node->range_c[0], node->range_c[1], node->n_numerousness, node->n_tm_numerousness, node->e_numerousness, node->e_tm_numerousness);

            printTabs(level);
            printf("Children:\n");
            print(node->child, level + 1);
        }
        node = node->sibling;
    }
}

void print_cc(node_tree *node, int level)
{
    while (node != NULL)
    {
        if (node->child != NULL)
        {
            printTabs(level);
            printf("Node: %d, [%d,%d], [n:%d,%d - e:%d,%d]\n", node->id, node->range_c[0], node->range_c[1], node->n_numerousness, node->n_tm_numerousness, node->e_numerousness, node->e_tm_numerousness);

            printTabs(level);
            printf("Children:\n");
            print_cc(node->child, level + 1);
        }
        node = node->sibling;
    }
}

void print_tree_recursive(node_tree *root, int level)
{
    printf("Root: %d, [%d,%d], [n:%d,%d - e:%d,%d]\n", root->id, root->range_c[0], root->range_c[1], root->n_numerousness, root->n_tm_numerousness, root->e_numerousness, root->e_tm_numerousness);
    printf("Children:\n");
    node_tree *node = root->child;
    print(node, level + 1);
}

void print_tree_cc_recursive(node_tree *root, int level)
{
    printf("Root: %d, [%d,%d], [n:%d,%d - e:%d,%d]\n", root->id, root->range_c[0], root->range_c[1], root->n_numerousness, root->n_tm_numerousness, root->e_numerousness, root->e_tm_numerousness);
    printf("Children:\n");
    node_tree *node = root->child;
    print_cc(node, level + 1);
}

int *count_components(graph *graph, node_tree *root, int level)
{

    int num_comp = 0;
    int num_vert = 0;
    int *result = malloc(sizeof(int) * 2);

    node_tree *node = root->child;

    node_tree **temp = malloc(sizeof(node_tree) * graph->len_v);
    int len = 0;
    int i = 0;

    int coreness_max = graph->edges->coreness;

    if (coreness_max < level)
    {
        printf("The coreness level is too high\n");
        printf("0 connected components\n");
        return 0;
    }

    while (1)
    {

        if (node->sibling != NULL)
        {
            temp[len] = node->sibling;
            len++;
        }
        if (node->child != NULL)
        {
            temp[len] = node->child;
            len++;

            if (node->parent->coreness != node->coreness)
            {
                if (node->coreness == level)
                {
                    num_comp++;
                }

                else if (node->parent->coreness < level && node->coreness > level)
                {
                    num_comp++;
                }
            }
        }
        else
        {
            if (node->coreness >= level)
            {
                num_vert++;
            }
        }

        if (len > i)
        {
            node = temp[i];
            i++;
        }
        else
        {
            result[0] = num_comp;
            result[1] = num_vert;
            free(temp);
            return result;
        }
    }
}

void count_components_all_levels(graph *graph, node_tree *root)
{   int max_k = graph->edges->coreness;
    int *result = malloc(sizeof(int) * 2);
    int num_comp = 0;
    int num_vert = 0;
    int temp = 0;

    for (int i = max_k; i > 0; i--)
    {
        result = count_components(graph, root, i);
        num_comp = result[0];
        num_vert = result[1];
        if (num_comp != temp)
        {
            printf("coreness: %d, number of connected components: %d, number of vertices: %d\n", i, num_comp, num_vert);
            printf("----------------------------------------------\n");
            printf("\n");
            temp = num_comp;
        }
    }
}

void find_id(graph *graph, node_tree *root, int id, int level)
{

    node_tree *node = graph->vertices[id].v_tree;
    node_tree *parent = node->parent;
    int coreness_max = graph->edges->coreness;

    if (level > coreness_max)
    {
        printf("Coreness level too high\n");
        return;
    }
    if (level > node->coreness)
    {
        printf("Node %d does not exist in coreness level %d\n", node->id, level);
        return;
    }

    while (1)
    {
        if ((parent->coreness == level) || ((parent->coreness > level) && (parent->parent->coreness < level)))
        {
            printf("The connected component to wich vertex %d belongs at coreness level %d has id = %d\n", id, level, parent->id);
            break;
        }
        if (parent == root)
        {
            printf("error");
            break;
        }
        parent = parent->parent;
    }
}

node_tree *find_component(graph *graph,node_tree *root, int id)
{

    node_tree *node = root->child;

    node_tree **temp = malloc(sizeof(node_tree) * graph->len_v);
    int len = 0;
    int i = 0;

    while (1)
    {

        if ((node->id == id) && (node->child != NULL))
        {
            return node;
        }

        if (node->sibling != NULL)
        {
            temp[len] = node->sibling;
            len++;
        }
        if (node->child != NULL)
        {
            temp[len] = node->child;
            len++;
        }

        if (len > i)
        {
            node = temp[i];
            i++;
        }
        else
        {
            printf("component not found\n");
            return root;
        }
    }
    free(temp);
}

void find_vertices_of_component(graph *graph, node_tree *root, int id)
{

    node_tree *node = find_component(graph, root, id);
    if (node != root)
    {
        printf("The vertices in the connected component %d are:\n", id);
        print(node->child, 1);
    }
}

void components_k_kp1(graph *graph, node_tree *root, int id)
{

    node_tree *node = find_component(graph, root, id);

    if (node == root)
        return;

    int k = node->coreness;

    printf("Coreness level %d\n", k);
    printf("\tConnected component id: %d\n", id);
    printf("\n");

    if (node->coreness == graph->edges->coreness)
    {
        printf("There is no connected component at coreness level %d\n", k + 1);
        return;
    }

    node_tree **children = malloc(sizeof(node_tree) * graph->len_v);
    int len = 0;

    node_tree *child = node->child;

    while (child->sibling != NULL)
    {
        if (child->child != NULL && child->coreness != child->parent->coreness)
        {
            children[len] = child;
            len++;
        }
        child = child->sibling;
    }
    if (child->child != NULL && child->coreness != child->parent->coreness)
    {
        children[len] = child;
        len++;
    }

    printf("len: %d\n", len);

    if (len == 0)
    {
        printf("Coreness level %d\n", k + 1);
        printf("Component %d disappears\n", id);
    }
    else if (len == 1)
    {
        printf("Coreness level %d\n", k + 1);
        if (children[0]->coreness == k + 1)
        {
            printf("The number of vertices decrease\n");
            printf("\tComponent id: %d\n", children[0]->id);
            printf("\n");
        }
        else
        {
            printf("The component %d at coreness level %d does not change\n", id, k + 1);
        }
    }
    else if (len >= 2)
    {
        printf("Coreness level %d\n", k + 1);
        printf("The component %d divides into:\n", id);
        for (int i = 0; i < len; i++)
        {
            printf("\tComponent: %d\n", children[i]->id);
        }
    }
    free(children);
}

void components_k_km1(graph *graph, node_tree *root, int id)
{

    node_tree *node = find_component(graph, root, id);
    int k = node->coreness;

    if (node == root)
        return;

    printf("Coreness level %d\n", node->coreness);
    printf("\tComponent id: %d\n", id);
    printf("\n");

    node_tree *parent = node->parent;

    if (parent->coreness == k - 1 && parent->coreness != 0)
    {

        node_tree **children = malloc(sizeof(node_tree) * graph->len_v);
        int len = 0;

        node_tree *child = parent->child;

        while (child->sibling != NULL)
        {
            if (child->child != NULL && child->coreness != child->parent->coreness)
            {
                children[len] = child;
                len++;
            }
            child = child->sibling;
        }
        if (child->child != NULL && child->coreness != child->parent->coreness)
        {
            children[len] = child;
            len++;
        }

        if (len == 1)
        {
            printf("Coreness Level %d:\n", k - 1);
            printf("Number of vertices increase\n");
            printf("\tComponent id: %d\n", parent->id);
            printf("\n");
        }
        else if (len >= 2)
        {
            printf("Coreness level %d:\n", k - 1);
            printf("The component %d merges with the following components of coreness level  %d and they form component %d:\n", id, k, parent->id);
            for (int i = 0; i < len; i++)
            {
                if (children[i]->id != id)
                {
                    printf("\tComponent id: %d\n", children[i]->id);
                }
            }
            printf("\n");
        }
        free(children);
    }
    else
    {
        printf("Coreness level %d\n", k - 1);
        printf("The component %d at coreness level %d does not change\n", id, k - 1);
    }
}

node_tree *max_common_coreness(graph *graph, int id_a, int id_b)
{

    int found_a = 0;
    int found_b = 0;

    vertex a;
    vertex b;

    node_tree *lca = NULL;

    if (graph->vertices[id_a].id == id_a)
    {
        a = graph->vertices[id_a];
        found_a = 1;
    }

    if (graph->vertices[id_b].id == id_b)
    {
        b = graph->vertices[id_b];
        found_b = 1;
    }

    if (found_a == 0 || found_b == 0)
    {
        printf("id not valid\n");
        exit(3);
    }

    node_tree *node_a = a.v_tree;
    node_tree *node_b = b.v_tree;

    node_tree *max_coreness_node = find_max_coreness(node_a, node_b);

    for (int j = 0; j <= max_coreness_node->coreness; j++)
    {
        if (node_a->parent->id == node_b->parent->id)
        {
            lca = node_a->parent;
            break;
        }
        if (node_a->parent->coreness < node_b->parent->coreness)
            node_b = node_b->parent;

        else if (node_b->parent->coreness < node_a->parent->coreness)
            node_a = node_a->parent;

        else
        {
            node_a = node_a->parent;
            node_b = node_b->parent;
        }
    }
    return lca;
}

void find_coreness(graph *graph, int id_a)
{
    printf("The maximum coreness of vertex %d is %d\n", id_a, graph->vertices[id_a].coreness);
}

void writeTabs(FILE *fp, int count)
{
    for (int i = 0; i < count; i++)
    {
        fprintf(fp, "\t");
    }
}

void write(node_tree *node, int level, FILE *fp)
{
    while (node && node->child != NULL)
    {
        writeTabs(fp, level);
        fprintf(fp, "{\"Node\": %d,\n", node->id);
        writeTabs(fp, level);
        fprintf(fp, " \"Coreness\": %d,\n", node->coreness);
        writeTabs(fp, level);
        fprintf(fp, " \"minCoreness\":  %d,\n", node->range_c[1]);
        writeTabs(fp, level);
        fprintf(fp, " \"maxCoreness\": %d,\n", node->range_c[0]);
        writeTabs(fp, level);
        fprintf(fp, " \"NumberOfVertices\": %d,\n", node->n_tm_numerousness);
        writeTabs(fp, level);
        fprintf(fp, " \"NumberOfEdges\": %d,\n", node->e_tm_numerousness);

        if (node->child != NULL)
        {
            writeTabs(fp, level);
            fprintf(fp, " \"Children\": [\n");
            write(node->child, level + 1, fp);
            writeTabs(fp, level);
            fprintf(fp, " ]\n");
            writeTabs(fp, level);
        }

        if (node->sibling && node->sibling->child)
            fprintf(fp, "},\n");
        else
            fprintf(fp, "}\n");
        node = node->sibling;
    }
}

void json_tree_recursive(node_tree *root, int level, FILE *fp)
{
    fprintf(fp, "\"Root\": %d,\n", root->id);
    fprintf(fp, " \"NumberOfVertices\": %d,\n", 0);
    fprintf(fp, " \"NumberOfEdges\": %d,\n", 0); 
    fprintf(fp, "\"Children\": [\n");
    node_tree *child = root->child;
    int child_printed = 0;
    while (child != NULL)
    {
        if (child->child != NULL)
        {
            write(child, level + 1, fp);
            child_printed = 1;
        }
        child = child->sibling;
        if (child != NULL && child->child != NULL && child_printed)
        {
            fprintf(fp, ",\n");
        }
    }
    fprintf(fp, " ]\n");
}

void output_json(node_tree *root, int level)
{
    FILE *fp;
    fp = fopen("output_complete.json", "w");
    fprintf(fp, "{");
    json_tree_recursive(root, level, fp);
    fprintf(fp, "}");
    fclose(fp);
}

void write_cc(node_tree *node, int level, FILE *fp)
{
    writeTabs(fp, level);
    fprintf(fp, "{\"Node\": %d,\n", node->id);
    writeTabs(fp, level);
    fprintf(fp, " \"Coreness\": %d,\n", node->coreness);
    writeTabs(fp, level);
    fprintf(fp, " \"minCoreness\":  %d,\n", node->range_c[1]);
    writeTabs(fp, level);
    fprintf(fp, " \"maxCoreness\": %d,\n", node->range_c[0]);
    writeTabs(fp, level);
    fprintf(fp, " \"NumberOfVertices\": %d,\n", node->n_tm_numerousness);
    writeTabs(fp, level);
    fprintf(fp, " \"NumberOfEdges\": %d,\n", node->e_tm_numerousness);
    writeTabs(fp, level);
    fprintf(fp, " \"children\": [\n");
    node_tree *child = node->child;
    int child_printed = 0;
    while (child != NULL)
    {
        if (child->child != NULL)
        {
            write_cc(child, level + 1, fp);
            child_printed = 1;
        }
        child = child->sibling;
        if (child != NULL && child->child != NULL && child_printed)
        {
            fprintf(fp, ",\n");
        }
    }
    writeTabs(fp, level);
    fprintf(fp, " ]}");
}

void json_tree_recursive_cc(node_tree *root, int level, FILE *fp)
{
    fprintf(fp, "\"Node\": %d,\n", root->id);
    fprintf(fp, "\"Coreness\": %d,\n", 0);
    fprintf(fp, "\"NumberOfVertices\": %d,\n", root->n_tm_numerousness);
    fprintf(fp, "\"NumberOfEdges\": 0,\n");
    fprintf(fp, "\"minCoreness\": 0,\n");
    fprintf(fp, "\"maxCoreness\": 0,\n");
    fprintf(fp, "\"children\": [\n");
    node_tree *child = root->child;
    int child_printed = 0;
    while (child != NULL)
    {
        if (child->child != NULL)
        {
            write_cc(child, level + 1, fp);
            child_printed = 1;
        }
        child = child->sibling;
        if (child != NULL && child->child != NULL && child_printed)
        {
            fprintf(fp, ",\n");
        }
    }
    fprintf(fp, " ]\n");
}

void output_json_cc(node_tree *root, int level, char *input_file)
{
    char *output_path = malloc(sizeof(char) * 300);
    strncpy(output_path, input_file, 299);
    // find pointer to last '/' in string
    char *lastslash = strrchr(output_path, '\\');
    if (lastslash)            // if found
        *(lastslash + 1) = 0; //   terminate the string right after the '/'

    output_path = strncat(output_path, "\\output.json", 280);
    printf("Saving in %s", output_path);
    FILE *fp;
    fp = fopen(output_path, "w");
    fprintf(fp, "{");
    json_tree_recursive_cc(root, level, fp);
    fprintf(fp, "}");
    fclose(fp);
}

int count_internal_nodes(node_tree *root, int len)
{
    node_tree *node = root->child;
    int size = len * 2;
    node_tree **temp = malloc(sizeof(node_tree) * size);

    if (temp == NULL)
    {
        printf("count_internal_nodes: malloc failed\n");
    }

    int len_t = 0;
    int i = 0;
    int result = 0;

    while (1)
    {

        if (node->sibling != NULL)
        {
            temp[len_t] = node->sibling;
            len_t++;
        }
        if (node->child != NULL)
        {
            result++;
            temp[len_t] = node->child;
            len_t++;
        }

        if (len_t > i)
        {
            node = temp[i];
            i++;
        }
        else
            break;
    }
    free(temp);

    return result;
}

int max_degree(graph *graph)
{
    int max = 0;
    int deg;
    for (int i = 0; i < graph->num_vertices; i++)
    {
        deg = graph->vertices[i].degree;
        if (deg > max)
        {
            max = deg;
        }
    }
    return max;
}

// Defined here to avoid warnings
void compact(node_tree *root, int len);

void primitives(graph *graph, node_tree *root, char *input_file)
{
    while (1)
    {
        int var;

        printf("\n------------------------------------------------------------PRIMITIVES-----------------------------------------------------------------\n\n");
        printf("On input graph:\n");
        printf("\t1 - Print the number of vertices and edges\n");
        printf("\t2 - Print list of the vertices with their coreness and neighbours list\n");
        printf("\t3 - Print list of the edges with their coreness\n");
        printf("\t4 - Print coreness of a specific vertex\n");
        printf("\t5 - Print minimum and maximum coreness values\n");
        printf("\nOn Core-Connectivity Tree:\n");
        printf("\t6 - Print Core-Connectivity Tree\n");              
        printf("\t7 - Print Core-Connectivity Tree without the leaves\n");
        printf("\t8 - Print the number of connected components and the number of vertices of a given coreness level\n");
        printf("\t9 - Print the number of connected components and the number of vertices for each coreness level\n");
        printf("\t10 - Given a vertex id and a coreness level, print the id of the connected component to which the vertex belongs at that coreness level\n");
        printf("\t11 - Print all the vertices that belong to a given connected component\n");
        printf("\t12 - Given the id of a connected component, at level k+1...\n");
        printf("\t\t does it remain the same as at level k\n");
        printf("\t\t the number of vertices decreases?\n");
        printf("\t\t does it divide into different connected components?\n");
        printf("\t\t does it disappear?\n");
        printf("\t13 - Given the id of a connected component, at level k-1...\n");
        printf("\t\t does it remain the same as at level k?\n");
        printf("\t\t the number of vertices increases?\n");
        printf("\t\t does it merge with different connected components?\n");
        printf("\t14 - Compute the maximum level of coreness for wich two given vertices belong to the same connected component\n");
        printf("\n\t15 - Compact the Core-Connectivity Tree\n");
        printf("\t16 - Save the Core-Connectivity Tree in output.json\n");              // ricorsiva
        printf("\t17 - Save the Core-Connectivity Tree without leaves in output.json\n"); // ricorsiva
        printf("\t18 - Count the number of internal nodes\n");                  // ricorsiva
        printf("\t19 - Print the maximum degree\n");
        printf("\nTo exit:\n");
        printf("\t20 - Exit\n");
        printf("\nInsert a value:\t");
        scanf("%d", &var);
        printf("\n");

        if (var == 1)
        {
            num_vertices_edges(graph);
        }
        else if (var == 2)
        {
            print_vertices(graph);
        }
        else if (var == 3)
        {
            print_edges(graph);
        }
        else if (var == 4)
        {
            int id_a;
            printf("Insert a vertex id to print its maximum coreness level:\n");
            scanf("%d", &id_a);
            find_coreness(graph, id_a);
        }
        else if (var == 5)
        {
            lv_coreness_max_min(graph);
        }
        else if (var == 6)
        {
            print_tree_recursive(root, 0);
        }
        else if (var == 7)
        {
            print_tree_cc_recursive(root, 0);
        }
        else if (var == 8)
        {
            int level;
            int *result = malloc(sizeof(int) * 2);
            printf("Insert the coreness level: ");
            scanf("%d", &level);
            printf("\n");
            result = count_components(graph, root, level);
            printf("There are %d connected components\n", result[0]);
            printf("The total number of vertices in the components is %d\n", result[1]);
        }
        else if (var == 9)
        {
            count_components_all_levels(graph, root);
        }
        else if (var == 10)
        {
            int id;
            int level;
            printf("Insert the vertex id: ");
            scanf("%d", &id);
            printf("Insert the coreness level: ");
            scanf("%d", &level);
            printf("\n");
            find_id(graph, root, id, level);
        }
        else if (var == 11)
        {
            int id;
            printf("Insert the connected component id: ");
            scanf("%d", &id);
            printf("\n");
            find_vertices_of_component(graph, root, id);
        }
        else if (var == 12)
        {
            int id;
            printf("Insert the connected component id: ");
            scanf("%d", &id);
            printf("\n");
            components_k_kp1(graph, root, id);
        }
        else if (var == 13)
        {
            int id;
            printf("Insert the connected component id: ");
            scanf("%d", &id);
            printf("\n");
            components_k_km1(graph, root, id);
        }
        else if (var == 14)
        {
            int id_a;
            int id_b;
            printf("\nFunction to find the maximum corenesse level such that two vertices belong to the same connected component\n");
            printf("Insert the id of the first vertex:\t");
            scanf("%d", &id_a);
            printf("Insert the id of the second vertex:\t");
            scanf("%d", &id_b);
            node_tree *lca = max_common_coreness(graph, id_a, id_b);
            printf("\nthe maximum corenesse level such that the two vertices belong to the same connected component is %d\n", lca->coreness);
        }
        else if (var == 15)
        {
            compact(root, graph->num_vertices);
        }
        else if (var == 16)
        {
            output_json(root, 0);
        }
        else if (var == 17)
        {
            output_json_cc(root, 0, input_file);
        }
        else if (var == 18)
        {
            int result = count_internal_nodes(root, graph->num_vertices);
            printf("The number of internal nodes is: %d\n", result);
        }
        else if (var == 19)
        {
            int result = max_degree(graph);
            printf("The maximum degree is: %d\n", result);
        }
        else if (var == 20)
        {
            free(graph->edges);
            free(graph->vertices);
            free(graph);
            exit(0);
        }
        else
        {
            printf("Value not valid\n");
        }
        printf("--------------------------------------------------------------------------------------------------------------------------------------\n\n");
    }
}

//****************************************************************MAIN****************************************************************//

void action_compact(node_tree *node, node_tree *parent, node_tree *first_child)
{

    node_tree *n_child = node->child;
    parent->e_tm_numerousness += node->e_tm_numerousness;
    parent->n_tm_numerousness += node->n_tm_numerousness;
    parent->range_c[0] = node->range_c[0];

    // 1. delete node from parent children
    if (first_child == node)
    {
        if (first_child->sibling == NULL)
        {
            parent->child = NULL;
            parent->last_child = NULL;
        }
        else
        {
            parent->child = first_child->sibling;
        }
    }
    else
    {
        node_tree *temp = first_child;
        while (temp->sibling != NULL)
        {
            if (temp->sibling == node)
            {
                if (temp->sibling->sibling == NULL)
                {
                    temp->sibling = NULL;
                    parent->last_child = temp;
                }
                else
                {
                    temp->sibling = temp->sibling->sibling;
                }
                break;
            }
            temp = temp->sibling;
        }
    }

    // 2. assign to node's children the node parent as parent
    while (n_child != NULL)
    {
        n_child->parent = parent;
        n_child = n_child->sibling;
    }

    // merge the children lists
    if (parent->child != NULL)
    {
        parent->last_child->sibling = node->child;
        parent->last_child = node->last_child;
    }
    else
    {
        parent->child = node->child;
        parent->last_child = node->last_child;
    }
}

void compact(node_tree *root, int len)
{

    node_tree *node = root->child;
    int size = len * 2;
    node_tree **temp = malloc(sizeof(node_tree) * size);
    node_tree **array = malloc(sizeof(node_tree) * len);

    if (temp == NULL || array == NULL)
    {
        printf("malloc compact failed\n");
    }

    int len_t = 0;
    int len_a = 0;
    int i = 0;

    root->depth = 0;
    root->coreness = 0;

    while (1)
    {

        if (node->sibling != NULL)
        {
            node->depth = node->parent->depth + 1;
            temp[len_t] = node->sibling;
            len_t++;
        }
        if (node->child != NULL)
        {
            node->depth = node->parent->depth + 1;
            temp[len_t] = node->child;
            len_t++;
        }

        if (node->depth % 2 == 0 && node->child != NULL)
        {
            printf("node: %d\n", node->id);
            array[len_a] = node;
            len_a++;
        }

        if (len_t > i)
        {
            node = temp[i];
            i++;
        }
        else
            break;
    }
    free(temp);

    //printf("\tnodes to compact %d\n", len_a);

    for (int j = 0; j < len_a; j++)
    {
        //if (j % 100 == 0 && j != 0)
        //    printf("\t%d\n", j);
        action_compact(array[j], array[j]->parent, array[j]->parent->child);
    }
    free(array);
}

void calculate_node_numerousness(graph *graph)
{

    for (int i = 1; i <= graph->num_vertices; i++)
    {
        node_tree *n = graph->vertices[i].v_tree;

        if (n->id != 0)
        {
            n->parent->n_tm_numerousness += 1;
            while (n->parent != NULL)
            {
                n->parent->n_numerousness += 1;
                n = n->parent;
            }
        }
    }
}

void calculate_edge_numerousness(graph *graph)
{
    for (int i = 0; i < graph->num_edges; i++)
    {
        vertex *a = graph->edges[i].a;
        vertex *b = graph->edges[i].b;
        node_tree *lca = max_common_coreness(graph, a->id, b->id);

        lca->e_tm_numerousness += 1;

        while (lca != NULL)
        {
            lca->e_numerousness += 1;
            lca = lca->parent;
        }
    }
}

void calculate_depth(graph *graph)
{
    for (int i = 1; i <= graph->num_vertices; i++)
    {
        node_tree *n = graph->vertices[i].v_tree;
        n->depth = 0;

        while (n->parent != NULL)
        {
            n->parent->depth = n->depth + 1;
            n = n->parent;
        }
    }
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("Please provide a string as a command-line argument.\n");
        return 1; // Exit the program with an error code
    }

    char *input_file = argv[1];

    // read data from file and setup the input structures
    graph *graph_input = set_input(input_file);
    // compute the coreness of the vertices
    compute_coreness(graph_input);

    // sort the edges of the graph by coreness value in decreasing order
    sort_edges(graph_input);
    
    // create the leaves of the CCTree and of the support structure
    init(graph_input);
    
    // build CCTree
    node_tree *root = build(graph_input);
  
    // merge nodes of the tree with parent-child relation if they have the same coreness
    fix_tree(root, graph_input->num_vertices);
  

    // count number of vertices in each component (node of the CCTree)
    calculate_node_numerousness(graph_input);
    
    // count number of vertices in each component (node of the CCTree)  -> not in linear time
   
    // total computation time
    
    output_json_cc(root, 0, input_file);

    return 0;
}
